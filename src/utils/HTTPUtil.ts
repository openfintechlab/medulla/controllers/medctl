/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Package for interacting with HTTP(s) using client libraries
 * @packageDocumentation
 */
import axios        from "axios";

 /**
  * Class for wrapping HTP requests
  */
export default abstract class HTTPUtil{
    
    /**
     * 
     * @param url (string) URL to do HTTP Post
     * @param request (any) JSON object to post to the endpoint
     */
    public static async doHTTPPost(url:string, request:any){
        let response = await axios.post(url,request);
        return response;        
    }

     /**
     * 
     * @param url (string) URL to do HTTP Post
     * @param request (any) JSON object to post to the endpoint
     */
    public static async doHTTPPut(url:string, request:any){
        let response = await axios.put(url,request);
        return response;        
    }

    /**
     * 
     * @param url (string) URL to do HTTP Post
     * @param request (any) JSON object to post to the endpoint
     */
    public static async doHTTPDel(url:string){
        let response = await axios.delete(url);
        return response;        
    }

     /**
     * 
     * @param url (string) URL to do HTTP Post
     * @param request (any) JSON object to post to the endpoint
     */
    public static async doHTTPGet(url:string){
        let response = await axios.get(url);
        return response;        
    }
}
/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: YAML File Loader Utility Project
 * *Description*
 * Utility class for loading YAML file
 * @packageDocumentation
 * - js-yaml
 *      - https://github.com/nodeca/js-yaml#readme
 *      
 */

import yaml         from "js-yaml"; 
import * as fs      from "fs";
import chalk        from "chalk";
import util         from "util";

/**
 * Abstract Utility class for loading YAML file
 * @abstract
 */
export default abstract class YAMLLoader{
    /**
     * 
     * @param path (string) File path for loading YAML file
     */
    public static loadYAML(path:string): any{
        console.info(chalk.blue(`+ Loading file: ${chalk.yellow(path)} `));                
        try{
            var doc = yaml.loadAll(fs.readFileSync(path, 'utf8'));
            console.info(chalk.green(`+ Loaded succesfully`));                
            return doc;
        }catch(error){
            console.error(chalk.redBright(`- Error while loading YAML file: ${util.inspect(error,{compact:true,colors:true, depth: null})} `));
            return undefined;
        }        
    }
}
/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Validator package container JOI validation rule 
 * @packageDocumentation
 */
import Joi      from "joi";
import util     from "util";


export default class RequestValidator{
    YAMLRequestObj = Joi.object({
        "apiVersion": Joi.string().min(3).max(32).required(),
        "name": Joi.string().min(8).max(64).required(),
        "apiEndpoint":{
            "host": Joi.string().hostname().required(),
            "contextRoot": Joi.string().uri({relativeOnly: true}).required(),
            "protocols": Joi.string().allow('http','https'),
            "port": Joi.number().port().optional(),
            "method": Joi.string().allow('GET','POST','PUT','DELETE')
        },
        "expectedResponse":{
            "codes": Joi.array().items(Joi.number()).required()
        },
        "payload": Joi.any()
    });

     /**
     * Validate the provided JSON object and provide the parsed schema for CREATE function
     * @example
     *  let sample:string = '........'; // JSON string
     *  try{
     *      let output = new ServiceMasterReqSchemaModel().validate(JSON.parse(sample));
     *      // here output = parsed JSON object
     *  }catch(error){
     *      console.log(error);
     *      // error:  {"metadata":{"status":"8154","description":"Schema validation error","trace":[{"source":"schema-validation","description":"service-master.name is required"}]}}        
     *  }        
     * @param req (Object) JSON object containing valid Schema
     */
    public validate(req:JSON){
        let {error,value} = this.YAMLRequestObj.validate(req);
        if(error){                        
            throw new Error(`Invalid Schema ${util.inspect(error,{compact:true,colors:true, depth: null})}` );
        }else{            
            return value;
        }
    }
}
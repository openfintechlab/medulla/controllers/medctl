/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * @Summary
 * Login controller for allowing user to login and with the specific API server.
 * @description
 * - [FB20210206]: Initial Commit
 */
import chalk                from "chalk";
import * as configJSON      from '../config/config.json';
import axios                from "axios";
import ConfigUtil           from "../config/Config.util";
import yaml                 from "js-yaml"; 

 /**
  * @summary Allows the user to Manage service versions
  * @classdesc ServiceVersionController manages all operations related to a service's versions
  */
 export default class ServiceVersionController{

    /**
     * Get all versions of the service
     * @param srvid Service ID to fetch all versions for
     */
    public getVersions(srvid:string){
        try{
            let configFile = ConfigUtil.instance().getConfigJSON();     
            console.log(srvid);
        
            let url: string = `${configFile.baseURL}/${configJSON.contextRoots.service_version}/`; 
            axios({
                method: "get",
                url: url,
                headers: {"Authorization": `Bearer ${configFile.JWT}`}
            })
            .then( response =>{
                console.table(response.data['service-version']);//.map(this.fileterOnlyReqEl));                   
            })
            .catch( error => {
                if(error.response){
                    if(error.response.data !== undefined && error.response.data.metadata.status !== '0000'){
                        console.error(chalk.red(`Error! while fetchind data from API server`) );
                        console.info(`Error Code: ${error.response.data.metadata.status} - Error Description: ${error.response.data.metadata.description}`);
                        if(error.response.data.metadata.status === `8904`){
                            console.info(`Please connect to the API server again by issuing \'connect\' command `);
                        }
                    }
                }else{
                    console.error(chalk.red(error.message));
                }                
            })
            
        }catch(error){
            // TODO! DO some here :-)
        }

    }

 }
/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * @Summary
 * Login controller for allowing user to login and with the specific API server.
 * @description
 * - [FB20210206]: Initial Commit
 */
import chalk                from "chalk";
import * as configJSON      from '../config/config.json';
import axios                from "axios";
import ConfigUtil           from "../config/Config.util";
import yaml                 from "js-yaml"; 


 /**
  * @summary Allows the user to Manage service
  * @classdesc ServiceController manages all operations related to a service
  */
 export default class ServiceController{

    /**
     * Get all services from service component
     * @param {string} id (optional) Service ID
     */
    public getServices(id?:string){
        // Call GET API of the service-master service-component
        try{
            // Step#1: Load configuration from the configuration file
            let configFile = ConfigUtil.instance().getConfigJSON();     
            let url: string = `${configFile.baseURL}/${configJSON.contextRoots.service_master}/`; 
            let byID = false;
            if(id){
                url += `${id}/`;
                byID = true;
            }         
            
            axios({
                method: "get",
                url: url,
                headers: {"Authorization": `Bearer ${configFile.JWT}`}
            })
            .then(response => {
                if(byID){                    
                    let ymlOutput = yaml.dump(response.data, {
                        flowLevel: 3,
                        styles: {
                          '!!int'  : 'hexadecimal',
                          '!!null' : 'camelcase'
                        }
                      });

                    console.log(ymlOutput);
                    
                }else{
                    console.table(response.data['service-master'].map(this.fileterOnlyReqEl));                   
                }                                    
            })
            .catch(error => {
                if(error.response){
                    if(error.response.data !== undefined && error.response.data.metadata.status !== '0000'){
                        console.error(chalk.red(`Error! while fetchind data from API server`) );
                        console.info(`Error Code: ${error.response.data.metadata.status} - Error Description: ${error.response.data.metadata.description}`);
                        if(error.response.data.metadata.status === `8904`){
                            console.info(`Please connect to the API server again by issuing \'connect\' command `);
                        }
                    }
                }else{
                    console.error(chalk.red(error.message));
                }                
            })            
        }catch(error){
            this.displayErrorOn(error);            
        }           
    }


    private fileterOnlyReqEl(item:any){
       return {
           "ServiceID": item.srv_id,
           "Name": item.name,
           "Hostname": item.hostname,
           "Port": item.port,
           "Lifecycle": item.lifecycle
       }  
    }

    private displayErrorOn(error:any){
        switch(error.message){
            case 'CONFIG_NOT_FOUND':
                console.error(chalk.red(`Configuration file not found. Please connect to the API server then issue this command again`));
                return;                                
            default:
                console.error(chalk.red(`Error while loading configuration file`));
                return;
        }
    }
 }
/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * @Summary
 * Login controller for allowing user to login and with the specific API server.
 * @description
 * - [FB20210206]: Initial Commit
 */
import * as fs          from "fs";
import inquirer         from "inquirer";
import chalk            from "chalk";
import * as configJSON   from '../config/config.json';
import axios            from "axios";


 /**
  * @summary Allows the user to login and creates a token file in the config folder
  * @classdesc LoginController for maanaging all aspects of token manamageement and login
  * NOTE: `$HOME/.oftl/config.json` file will be overwritten in-case login is called again
  */
 export default class LoginController{
    private baseURL: string;
    private userID!: string;
    private password!: string;

    /**
     * Instantiates `Login` controller
     * @param {string} baseURL Base URL of the endpoint
     */
    constructor(baseURL: string){
        if(!baseURL){
            throw new Error('INVLIAD_BASE_URL');
        }
        this.baseURL = baseURL;
    }

    /**
     * AAuthenticates the customer by passing the credentials to the API server and 
     * saves the rerived tokens in the config file.
     * @param {string} userID (optional)
     * @param {string} password (optional)
     */
    public login(userID?: string, password?:string){        
        try{            
            // Step#1: Prompt for user id password and 
            //         initiate login process. Once token is received in response,
            //         save it in the configuration file and provide prompt accordingly
            if(userID && password){
                this.userID = userID;
                this.password = password;
            }else{
                let asyncProcess = async() => {
                    await this.promptUidAndPass();            
                    let respoObj = await this.getCredentials();
                    // Step#2: Save the tokens along with endpoint details in the config.json
                    //         file under folder $HOME/.oftl/config.json
                    await this.writeTokenConfig(respoObj);
                }                
                asyncProcess()
                .then(() => {
                    console.info(chalk.green(`Connected Established with: ` ) + `${this.baseURL}`);                    
                })
                .catch(error => {
                    if(error.message === 'INVALID_USER'){
                        console.error(chalk.red(`${error.message}! `) + `Error while authenticating user.`);
                    }else{
                        console.error( chalk.red(`Error!`) +`Error while while connecting to the API server`);
                    }                                                
                });
            }            
        }catch(error){                   
            console.error(`Error while authenticating user.`);            
        }
        
    }

    /**
     * Loads Config File and Validates the content
     * @returns {object | undefined} JSON object with the configuration OR undefined in-case file does not exist
     */
    public loadAndValidateOFTLConfigFile(): any{
        let homeDIR = process.env.HOME;
        
        homeDIR = `${homeDIR}/${configJSON.defaults.configDir}/${configJSON.defaults.configFile}`;
        try{
            let config = fs.readFileSync(homeDIR,'utf-8');
            let configObj = JSON.parse(config);
            if(configObj === undefined || configObj.JWT === ''){
                throw new Error('INVALID_CONFIG');
            }
            return config;
        }catch(error){
            console.log(chalk.red(`File ${homeDIR} does not exist.`))
            return undefined
        }        
    }

    private async promptUidAndPass(){
        // Take input from the user    
        const requireLetterAndNumber = (value: any) => {
            if (/\w/.test(value) && /\d/.test(value)) {
              return true;
            }
          
            return 'Password need to have at least a letter and a number';
          }; 
          let answers = await inquirer.prompt([
                  {
                    type: 'input',
                    message: 'Enter a username: ',
                    name: 'username'
                  },
                  {
                    type: 'password',
                    message: 'Enter a masked password: ',
                    name: 'password',
                    mask: '*',
                    validate: requireLetterAndNumber,
                  },
                ]);
                this.userID = answers.username;
                this.password = answers.password; 
    }

    private async getCredentials(){
        // Gets credentials by calling the specific endpoint        
        try{
            let response = await axios.post(  `${this.baseURL}/${configJSON.contextRoots.login}/${configJSON.defaults.appName}`,
                    {/** data */
                        "userDetails":{
                            "username": this.userID,
                            "password": this.password
                        }
                    },
                    { /** config */
                        timeout: 5000,
                        headers: {'Content-Type': 'application/json'}
                    }
            );
            if(response.data.metadata !== undefined && response.data.metadata.status !== '0000'){
                console.error(`Error while connecting with the API server`);
                console.error(`Error Code:`, `${response.data.metadata.status}`, `Status:`, `${response.data.metadata.description}`);
                throw new Error('INVALID_RESPONSE');
            }
            return response.data;
        } catch(error){
            if(error.response.data.metadata !== undefined && error.response.data.metadata.status !== '0000'){
                console.error(chalk.red(`Error while connecting with the API server`));
                console.error(chalk.redBright(`Error Code:`), `${error.response.data.metadata.status}`, chalk.redBright(`Status:`), `${error.response.data.metadata.description}`);                
            }
            throw new Error('INVALID_USER');            
        }
                      
    }

    private async writeTokenConfig(loginRespJSON: any){
        let homeDIR = process.env.HOME;        
        homeDIR = `${homeDIR}/${configJSON.defaults.configDir}/${configJSON.defaults.configFile}`;
        try{
            let fileContentJSON = {
                "JWT": loginRespJSON.tokenDetails.access_token,
                "baseURL": this.baseURL,
                "refreshToken": loginRespJSON.tokenDetails.access_token,
                "sessionState": loginRespJSON.tokenDetails.session_state,
                "userUUID": loginRespJSON.tokenDetails.userID
            };
            this.createFolderIfNotExist();            
            fs.writeFileSync(homeDIR,JSON.stringify(fileContentJSON),'utf-8')
        }catch(error){
            throw error;
        }   
    }

    private createFolderIfNotExist(): boolean{
        let homeENV = process.env.HOME; 
        try{                            
            fs.accessSync(`${homeENV}/${configJSON.defaults.configDir}/${configJSON.defaults.configFile}`,fs.constants.R_OK | fs.constants.W_OK);
            return false; // Folder exist and not created
        }catch(error){
            fs.mkdirSync(`${homeENV}/${configJSON.defaults.configDir}/`, {recursive: true});
            return true; // Folder does not exist and created
        }
    }
 }
/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Main entry point of the solution
 * <b>Description</b>
 * - Root entry level file for bootstarting node js application
 * - Load commadn line arguments 
 * @packageDocumentation
 */
import * as yargs       from 'yargs';
import chalk            from "chalk";
import YAMLLoader       from './utils/YAMLLoader';
import RequestValidator from './validators/Request.validator';
import util             from "util";
import HTTPUtil         from './utils/HTTPUtil';
import LoginController  from "./controllers/LoginController"
import * as confgJSON from './config/config.json';
import ServiceController from './controllers/ServiceController';
import ServiceVersionController from './controllers/ServiceVersionController';

/**
 * Claass for Bootstarting the solution
 * @abstract
 */
export default class BootStrap{    
    private args:any;
    

    public get cmdArgs(): any{
        return this.args;
    }

    constructor(){
        
        this.args = yargs
                        .scriptName('medctl')
                        .command('connect [baseURL]', 'Connects with the medulla API server', (args) => {
                            args.positional('baseURL', {
                            describe: 'Base URL fo the API server',
                            default: confgJSON.defaults.baseURL,
                            type: 'string'
                            })
                        }, (argv) => {                            
                            let loginController: LoginController = new LoginController( <string>argv.baseURL );
                            loginController.login(); // Class will prompt for userid and password                            
                        })
                        .command(`add <type>`, `Adds a new service, version, endpoint`, (args) => {
                            args.positional('type', {
                            describe: 'Type of the entity',
                            choices: ['service', 'version', 'endpoint', 'owner'],
                            type: 'string'
                            })
                        }, (argv) => {
                            console.log(`Required params for command add found`);
                            console.log(`${argv}`);
                        })
                        .command(`update <type>`, `Updates a new service, version, endpoint`, (args) => {
                            args.positional('type', {
                            describe: 'Type of the entity',
                            choices: ['service', 'version', 'endpoint', 'owner'],
                            type: 'string'
                            })
                        }, (argv) => {
                            console.log(`Required params for command update found`);
                            console.log(`${argv}`);
                        })
                        .command(`delete <type>`, `Deletes existing service, version, endpoint`, (args) => {
                            args.positional('type', {
                            describe: 'Type of the entity',
                            choices: ['service', 'version', 'endpoint', 'owner'],
                            type: 'string'
                            })
                        }, (argv) => {
                            console.log(`Required params for command delete found`);
                            console.log(`${argv}`);
                        })
                        .command(`get <type>`, `Get existing service, version, endpoint`, (args) => {
                            args.positional('type', {
                            describe: 'Type of the entity',
                            choices: ['service', 'version', 'endpoint', 'owner'],
                            type: 'string'
                            }).options({
                                'serviceid': {                                    
                                    demandOption: false,
                                    describe: "Service ID to fetch the records for",
                                    type: 'string'
                                }                                
                            })
                        }, (argv) => {   
                            this.processGetOption(<string> argv.type, {serviceid: argv.serviceid});                                                                       
                        })                        
                        .command(`disconnect`, `Disconnects from the API server and clears the tokens`, (_) => {
                           
                        }, (argv) => {
                            console.log(`Required params for command getting specific service component`);
                            console.log(`${argv}`);
                        })                        
                        .demandCommand()
                        .help()
                        .argv
                        
    }

    /**
     * Load YAML File
     * @param filePath (string) Absolute location of the file
     */
    public loadYAMLFile(filePath:string):JSON[]{
        let docs = YAMLLoader.loadYAML(filePath);
        if(docs.length > 0){
            console.info(chalk.blue(`+ Docs parsed: ${chalk.green(docs.length)} `));            
            return docs;
        }else{
            console.error(chalk.redBright(`- Invalid YAML. No document found`));
            throw new Error(`- Invalid YAML. No document found`);
        }        
    }

    /**
     * Process docs
     * @param docs (JSON)
     */
    public async processDoc(docs:JSON[]){                        
        for(let i=0;i<docs.length;i++){
            let element:any = docs[i];
            console.log(chalk.blue(`+ Validating: ${chalk.green(element.name)}`));
            try{
                let output = new RequestValidator().validate(element);                
                await this.callSpecificHTTPMethod(output);
            }catch(error){
                console.log(`- Error while validating the schema: ${error}`);
            }            
        }
    }

    private async callSpecificHTTPMethod(doc:any){
        let url:string = doc.apiEndpoint.protocols + "://"+ doc.apiEndpoint.host + ":" +
                            ((doc.apiEndpoint.port)?doc.apiEndpoint.port : "") + "/" + doc.apiEndpoint.contextRoot;                            
        console.info(chalk.blue(`+ Calling URL:  ${chalk.yellow(url)} `));
        console.log(`\t HTTP Method: ${chalk.yellow(doc.apiEndpoint.method)}`);
        try{
            switch(doc.apiEndpoint.method){
                case 'POST':{
                    console.log(`\t${chalk.grey('+ Calling Endpoint')}`);
                    let response = await HTTPUtil.doHTTPPost(url,doc.payload);
                    console.info(`\t${chalk.grey('+ Response received: ')} ${chalk.greenBright(response.data.metadata.status)} | ${chalk.greenBright(response.data.metadata.description)}`);
                    break;
                }
                case 'PUT':{
                    console.log(`\t${chalk.grey('+ Calling Endpoint')}`);
                    let response = await HTTPUtil.doHTTPPut(url,doc.payload);
                    console.info(`\t${chalk.grey('+ Response received: ')} ${chalk.greenBright(response.data.metadata.status)} | ${chalk.greenBright(response.data.metadata.description)}`);
                    break;
                }         
                case 'DEL':{
                    console.log(`\t${chalk.grey('+ Calling Endpoint')}`);
                    let response = await HTTPUtil.doHTTPDel(url);
                    console.info(`\t${chalk.grey('+ Response received: ')} ${chalk.greenBright(response.data.metadata.status)} | ${chalk.greenBright(response.data.metadata.description)}`);
                    break;
                }
                case 'GET':{
                    console.log(`\t${chalk.grey('+ Calling Endpoint')}`);
                    let response = await HTTPUtil.doHTTPGet(url);
                    if(response.data){
                        // console.table(response.data);
                        console.log(`${util.inspect(response.data, {showHidden: false, depth: null})}`)
                    }
                    
                    console.info(`\t${chalk.grey('+ Response received: ')} ${chalk.greenBright(response.data.metadata.status)} | ${chalk.greenBright(response.data.metadata.description)}`);
                    break;
                }
            }
        }catch(error){            
            if(error.response){
                console.error(`\t${chalk.red('- Error while calling endpoint:')}  ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`);                
            }else{                
                console.error(`\t${chalk.red('- Error while calling endpoint:')}  ${util.inspect(error.message,{compact:true,colors:true, depth: null})}`);
            }            
        }
    }

    private processGetOption(type:string, option:any){
        switch(type.toUpperCase()){
            case 'SERVICE':
                new ServiceController().getServices(option.serviceid)
                break;
            case 'VERSION':
                new ServiceVersionController().getVersions(option.serviceid);

        }
    }

}

/**
 * initiate the call
 */
var bootStarter:BootStrap = new BootStrap(); // Will call constructor which will initiate teh 
                                             // the call to the yargs

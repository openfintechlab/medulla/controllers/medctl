/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * @Summary
 * Login controller for allowing user to login and with the specific API server.
 * @description
 * - [FB20210206]: Initial Commit
 */

import * as configJSON   from '../config/config.json';
import * as fs          from "fs";
import chalk            from "chalk";

 export default class ConfigUtil{
    private static configUtil: ConfigUtil = new ConfigUtil();    
    private _CONFIG_FILE!: string;

    // Singleton
    private constructor(){        
        this._CONFIG_FILE = `${process.env.HOME}/${configJSON.defaults.configDir}/${configJSON.defaults.configFile}`;
    }

    
    /**
     * Get isntance of ConfigUtil
     * @returns {ConfigUtil} Instance of ConfigUtil
     */
    public static instance():ConfigUtil{
        if(this.configUtil){
            this.configUtil = new ConfigUtil();            
        }
        return this.configUtil;
    }

    /**
     * Get configuration JSON object from file $HOME/.oftl/config.json
     */
    public getConfigJSON(){
        // Step#1: Check if config file exist
        try{
            if(this.configExist()){
                // Step#2: Load configuration file and read token fields
                let configData = this.readContentOfConfig();
                let configJSON = JSON.parse(<string> configData)
                return configJSON;
            }
        }catch(error){            
            throw error;
        }        
    }

    private configExist(){
        try{                                                        
            fs.accessSync(this._CONFIG_FILE,fs.constants.R_OK | fs.constants.W_OK);                        
            return true;
        }catch(error){            
            throw new Error(`CONFIG_NOT_FOUND`);            
        }
    }

    
    private readContentOfConfig(): string | undefined{
        try{
            let data = fs.readFileSync(this._CONFIG_FILE, 'utf-8');
            return data;
        }catch(error){
            throw new Error('UNABLE_TO_READ_CONFIG');
        }
    }

    private displayErrorOn(error:any){
        switch(error.message){
            case 'CONFIG_NOT_FOUND':
                console.error(chalk.red(`Configuration file not found. Please connect to the API server then issue this command again`));
                return;                                
            default:
                console.error(chalk.red(`Error while loading configuration file`));
                return;
        }
    }

 }